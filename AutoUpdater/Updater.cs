﻿using System;
using System.Net;

namespace AutoUpdater
{
    class Updater
    {
        void Update(string DownloadPath)
        {
            string[] args = Environment.GetCommandLineArgs();
            WebClient wc = new WebClient();
            wc.DownloadFile(DownloadPath, "File.exe");
        }
    }
}
