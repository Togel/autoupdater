﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Xml;

namespace Updater
{
    internal class Version
    {
        public Version(string VersionString)
        {
            string[] Versions = VersionString.Split('.');

            if (Versions.Length == 3)
            {
                Major = Convert.ToUInt32(Versions[0]);
                Minor = Convert.ToUInt32(Versions[1]);
                Revision = Convert.ToUInt32(Versions[2]);
            }
        }

        public static bool operator <(Version v1, Version v2)
        {
            if (v1.Major < v2.Major)
            {
                return true;
            }
            else if (v1.Major == v2.Major)
            {
                if (v1.Minor < v2.Minor)
                {
                    return true;
                }
                else if (v1.Minor == v2.Minor)
                {
                    return v1.Revision < v2.Revision;
                }
            }

            return false;
        }

        public static bool operator >(Version v1, Version v2)
        {
            if (v1.Major > v2.Major)
            {
                return true;
            }
            else if (v1.Major == v2.Major)
            {
                if (v1.Minor > v2.Minor)
                {
                    return true;
                }
                else if (v1.Minor == v2.Minor)
                {
                    return v1.Revision > v2.Revision;
                }
            }

            return false;
        }

        uint Major = 0;
        uint Minor = 0;
        uint Revision = 0;
    }

    class Program
    {
        /// Call Updater with following Arguments:
        /// 0: Process ID.
        /// 1: Name of the Executable which is updated.
        /// 2: Current Version of the Executable which is updated.
        /// 3: Path of the Executable which is updated.
        /// 4: Download Path to the Changelog File including a XML.
        /// 5: Download Path to the Zip File containing the Update.
        static void Main(string[] Arguments)
        {
            using (WebClient Client = new WebClient())
            {
                // Parse Command Line.
                int ProcessID = Convert.ToInt32(Arguments[0].ToString());
                string ExecutableName = Arguments[1].ToString();
                string DirectoryPath = Arguments[3].ToString();
                string DownloadPath = Arguments[5].ToString();
                string ChangelogPath = DirectoryPath + @"\changelog.xml";
                string UpdatePath = DirectoryPath + @"\update.zip";

                // Verion Check.
                Client.DownloadFile(Arguments[4].ToString(), ChangelogPath);

                XmlDocument VersionXML = new XmlDocument();
                VersionXML.Load(ChangelogPath);
                File.Delete(ChangelogPath);

                Version AppVersion = new Version(Arguments[2].ToString());
                Version UpdateVersion = new Version(VersionXML.DocumentElement.FirstChild.Attributes["Version"].Value);

                if (UpdateVersion > AppVersion)
                {
                    // Update.
                    Process.GetProcessById(Convert.ToInt32(ProcessID)).Kill();

                    Client.DownloadFile(DownloadPath, UpdatePath);

                    using (ZipArchive Archive = ZipFile.OpenRead(UpdatePath))
                    {
                        foreach (ZipArchiveEntry Entry in Archive.Entries)
                        {
                            string EntryPath = Path.GetFullPath(Path.Combine(DirectoryPath, Entry.FullName));

                            if (Entry.Name.Length == 0)
                            {
                                Directory.CreateDirectory(EntryPath);
                            }
                            else
                            {
                                Entry.ExtractToFile(EntryPath, true);
                            }
                        }
                    }

                    File.Delete(UpdatePath);

                    Process.Start(DirectoryPath + @"\" + ExecutableName);
                }
            }
        }
    }
}
